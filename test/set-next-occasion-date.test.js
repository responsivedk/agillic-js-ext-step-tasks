/* global describe it */
const chai = require('chai')
const expect = chai.expect
const executor = require('../lib/executor')

describe('Set next occasion date', function () {
  it('Basic case', async function () {
    let result = await executor('set-next-occasion-date/step.js', 'set-next-occasion-date/type.definition.json', 'set-next-occasion-date/data-sets/basic.json')
    expect(result.result).to.equal(true)
    expect(result.otmData).to.be.an('object').with.property('OCCASIONS')
    expect(result.otmData.OCCASIONS).to.have.all.keys('BIRTHDAY-1', 'BIRTHDAY-3', 'VALENTINES_DAY', 'MOTHERS_DAY', 'EASTER_SUNDAY')

    expect(result.otmData.OCCASIONS['BIRTHDAY-1'].NEXT_DATE).to.equal('01.01.2020')
    expect(result.otmData.OCCASIONS['BIRTHDAY-3'].NEXT_DATE).to.equal('05.05.2019')
    expect(result.otmData.OCCASIONS['VALENTINES_DAY'].NEXT_DATE).to.equal('14.02.2020')
    expect(result.otmData.OCCASIONS['MOTHERS_DAY'].NEXT_DATE).to.equal('12.05.2019')
    expect(result.otmData.OCCASIONS['EASTER_SUNDAY'].NEXT_DATE).to.equal('21.04.2019')
  })
})
