# Usage

## Prerequisites

- Node.js 8+
- JRE 8+


## Installation

```
npm install
npm run download-runtime
```

## Executing tests

```
npm run test:<task-name>

// E.g.
npm run test:set-next-occasion-date
```

# Agillic JS extension steps

JS ext. steps are used in Agillic to implement custom business logic for manipulating data in the Agillic platform.

[Agillic JS ext. step documentation](https://developers.agillic.com/jsextensions)

The particular JS runtime uses Oracle's [Nashorn](https://blogs.oracle.com/nashorn/) JS engine. The Java-JS bridge leaks quite a bit and it seems like most values are not JS primitives, but Java objects with JS bindings.

# Task: Set next occasion date

## Background

The occasion's site provides users with the ability to set up reminders for various occasions.

The occasions have type, either individual or common.
Individual occasions are stand-alone. For example birthdays or anniversaries.
Common occasions are occasions where all users' occasion will have the same date (for a given year). For example Mother's Day or Christmas Day.

Common occasions can be further split into fixed and non-fixed in regards to their recurrence date. Christmas Day is always December 25th and is thus fixed, while Mother's day is the second Sunday of May and is thus non-fixed.

As some non-fixed occasions are non-trivial to calculate (e.g. Easter Sunday), a lookup table is populated with the dates for a range of years for each non-fixed occasion type.

## Data model

E/R-style diagram of the data in question:

```plantuml
@startuml
hide circle
hide empty members
package PersonData {
  entity Recipient {
    * ID : string
  }
  entity OCCASIONS {
    * ID : string
    --
    * OCCASION_TYPE_ID : string
    MONTH : number
    DAY : number
    NEXT_DATE : date
  }
  note right: One-to-many table
}
package GlobalData {
  entity OCCASION_TYPES {
    * ID : string
    --
    TYPE : string
    IS_NON_FIXED : boolean
    MONTH : number
    DAY : number
  }
  note right: Global data table
  entity OCCASION_TYPE_NON_FIXED_DATES {
    * ID : string
    --
    * OCCASION_TYPE_ID : string
    YEAR : number
    MONTH : number
    DAY : number
  }
  note right: Global data table
}
Recipient ||--o{ OCCASIONS
OCCASIONS }--|| OCCASION_TYPES
OCCASION_TYPES ||--o{ OCCASION_TYPE_NON_FIXED_DATES
@enduml
```

Explore the type definition (`set-next-occasion-date/type.definition.json`) and data file (`set-next-occasion-date/data-sets/basic.json`)

## Exercise

> **tl;dr** Implement the `execute` method in `set-next-occasion-date/step.js` and get the test pass.

The simulated user has different types of occasions and occasions is different states of validity in regard to `NEXT_DATE` (empty, expired, already correct).

The task is to update `NEXT_DATE` for occasions with an expired date (i.e. a date in the past) or missing date (i.e. `NEXT_DATE` is not set).

The setting `EXECUTION_DATE` should be used as the reference date (as opposed to using the current date - makes testing easier).
