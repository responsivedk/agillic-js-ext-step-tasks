const path = require('path')
const exec = require('child_process').exec
const moment = require('moment')
const jarPath = path.join(__dirname, 'standalone.jar')

const parseStandaloneOutput = function (output, typePath) {
  let types = require(path.join(__dirname, '..', typePath))
  let result = {
    result: false,
    errors: [],
    personData: {},
    otmData: {},
    events: []
  }
  let pattern = /^\[(.+?)\]: ?(.+?)$/
  let personDataPattern = /^\t(.+?) : (.+?)$/
  let otmDataPattern = /^\t\t\t(.+?)=(.+?)$/
  let stage = null
  let otmTable = null
  let otmEntry = null
  output.trim().split('\n').forEach(line => {
    let m = line.match(pattern)
    if (!m) {
      return
    }
    switch (m[2]) {
      case 'Modified Person Data:':
        stage = 'personData'
        return
      case 'Modified OTM data:':
        stage = 'otmData'
        return
      case 'Achieved events:':
        stage = 'events'
        return
      case '---------------------- Result true --------------------':
        result.result = true
        return
      case '---------------------- Result false --------------------':
        result.result = false
        return
    }
    if (m[1] === 'ERROR') {
      result.errors.push(m[2])
      return
    }
    switch (stage) {
      case 'personData':
        let s = m[2].match(personDataPattern)
        if (s) {
          let type = types.recipient[s[1]] || 'string'
          let v
          switch (type) {
            case 'date':
              v = moment(s[2], 'ddd MMM DD HH:mm:ss ZZZ YYYY').format('DD.MM.YYYY')
              break
            case 'timestamp':
              v = moment(s[2], 'ddd MMM DD HH:mm:ss ZZZ YYYY').format('DD.MM.YYYY HH:mm:ss')
              break
            case 'number':
              v = Number(s[2])
              break
            case 'string':
            default:
              v = s[2]
              break
          }
          result.personData[s[1]] = v
        }
        break
      case 'otmData':
        if (/^\t[A-Z][A-Z0-9_]+:$/.test(m[2])) {
          otmTable = m[2].substr(1, m[2].length - 2)
          otmEntry = -1
          result.otmData[otmTable] = []
        } else if (m[2] === '\t\tRecord ADDED') {
          otmEntry++
          result.otmData[otmTable][otmEntry] = {
            _operation: 'insert'
          }
        } else if (m[2] === '\t\tRecord MODIFIED') {
          otmEntry++
          result.otmData[otmTable][otmEntry] = {
            _operation: 'update'
          }
        } else {
          let s = m[2].match(otmDataPattern)
          if (s) {
            let type = types.recipient[otmTable][s[1]] || 'string'
            let v
            switch (type) {
              case 'date':
                v = moment(s[2], 'ddd MMM DD HH:mm:ss ZZZ YYYY').format('DD.MM.YYYY')
                break
              case 'timestamp':
                v = moment(s[2], 'ddd MMM DD HH:mm:ss ZZZ YYYY').format('DD.MM.YYYY HH:mm:ss')
                break
              case 'number':
                v = Number(s[2])
                break
              case 'string':
              default:
                v = s[2]
                break
            }
            result.otmData[otmTable][otmEntry][s[1]] = v
          }
        }
        break
      case 'events':
        break
    }
  })
  Object.keys(result.otmData).forEach(table => {
    let idField = types.recipient[table].prim

    result.otmData[table] = result.otmData[table].reduce((a, e) => {
      a[e[idField]] = e
      return a
    }, {})
  })
  return result
}

module.exports = function (scriptPath, typePath, dataPath) {
  return new Promise(function (resolve, reject) {
    exec(`java -jar ${jarPath} ${scriptPath} ${typePath} ${dataPath}`, function (err, stdout, stderr) {
      console.log(stdout)
      if (err) {
        reject(err)
      } else {
        resolve(parseStandaloneOutput(stdout, typePath))
      }
    })
  })
}
