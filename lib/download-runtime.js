const fs = require('fs')
const path = require('path')
const https = require('https')

const jarUrl = 'https://developers.agillic.com/extension-lib/standalone.jar'

const libPath = path.join(__dirname, 'standalone.jar')
// Check if jar is already there
if (!fs.existsSync(libPath)) {
  const file = fs.createWriteStream(libPath)
  https.get(jarUrl, function (response) {
    response.pipe(file)
    console.log(`Done`)
  })
} else {
  console.log('Already downloaded')
}
